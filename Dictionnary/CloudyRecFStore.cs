using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Hashtable = System.Collections.Generic.Dictionary<object, object>;
using ArrayList = System.Collections.Generic.List<object>;

public class CloudyRecFStore : CloudyRecResource
{
    public string fileName { get; set; }
    public string contentType { get; set; }
    /**
      * Default constructor
      */
    public void CloudyRecFstore() { }
    public void CloudyRecFstore(string fileName, string contentType) 
    {
        this.fileName = fileName;
        this.contentType = contentType;
    }

    /**
	 * Upload given file to FStore
	 * @param data Array of bytes of file to upload.
	 */
    public void upload(byte[] data, Func<RestResponse, int> callbackFunction = null)
    {
        contentType = contentType == null ? "application/octet-stream" : contentType;

        if (id == null || id == "")
        {
            if(callbackFunction == null)
                CloudyRecConnector.Instance.httpPostMultipart(GetAppKey(), "fstore", id, data, fileName, contentType, callbackUpload);
            else
                CloudyRecConnector.Instance.httpPostMultipart(GetAppKey(), "fstore", id, data, fileName, contentType, callbackFunction);
        }
        else
        {
            if (callbackFunction == null)
                CloudyRecConnector.Instance.httpPutMultipart(GetAppKey(), "fstore", id, data, fileName, contentType, callbackUpload);
            else
                CloudyRecConnector.Instance.httpPutMultipart(GetAppKey(), "fstore", id, data, fileName, contentType, callbackFunction);
        }
	}

    /**
	 * Get first page of all Fstore items with limit 10.
	 * @return
	 */

    public void FilesList(int limit, int page, Func<RestResponse, int> callbackFunction)
    {
        if (limit < 0)
            limit = 10;
        if (page <= 0)
            page = 1;

        Hashtable data = new Hashtable();
        data.Add("limit", limit);
        data.Add("page", page);

        CloudyRecConnector.Instance.HttpGet(GetAppKey(), "fstore/list", null, data, callbackFunction);
    }

    public void FilesList(Func<RestResponse, int> callbackFunction = null)
    {
        FilesList(10, 1, callbackFunction);
    }	
	
    ///**
    // * Set tags for given Fstore record id.
    // * @param id Id of fstore record for tag setting.
    // * @param tags Array of strings.
    // * @return Id of updated record.
    // */
    public void SetTags(String id, ArrayList tags, Func<RestResponse, int> callbackFunction = null) 
    {
        Hashtable data = new Hashtable();

        string strTags = "";
        foreach(string tag in tags)
            strTags += tag + ",";
        
        data.Add("tags", strTags);
        CloudyRecConnector.Instance.HttpPut(GetAppKey(), "fstore", id + "/tags", data, callbackFunction);
    }
	
    /**
     * Get tags for given Fstore record id.
     * @param id Id of fstore record for tag retrieval.
     * @return Array of strings as tags.
     */
    public void GetTags(String id, Func<RestResponse, int> callbackFunction = null) 
    {
        if (id == null || id == "")
        {
            Debug.WriteLine("You need to specify the id to get the tags");
        }
        else
        {
            CloudyRecConnector.Instance.HttpGet(GetAppKey(), "fstore", id + "/tags", null, callbackFunction);
        }
    
    }

    //Return the URL of the file with the given id
    //Useful for setting image source directly with the id
    public string GetFileURL(string id)
    {
        return CloudyRecConnector.Instance.GetFileUrl(GetAppKey(), id);
    }
	
    /**
     * Download file of given FStore record.
     * @param id Id of fstore record for download.
     * @return Array of byte data downloaded.
     */
    public void Download(String id, Func<RestResponse, int> callbackFunction = null)
    {
        if (callbackFunction == null)
            CloudyRecConnector.Instance.HttpGet(GetAppKey(), "fstore", id, null, callbackDownload);
        else
            CloudyRecConnector.Instance.HttpGet(GetAppKey(), "fstore", id, null, callbackFunction);
    }
	
    /**
     * Delete Fstore record of given id.
     * @param id Id of fstore record to delete.
     * @return True if successful.
     */
    public void Delete(string id, Func<RestResponse, int> callbackFunction = null)
    {
        this.id = id;
        if (this.id != null && this.id != "")
        {
            if(callbackFunction == null)
                CloudyRecConnector.Instance.HttpDelete(GetAppKey(), "fstore", id, null, callbackDelete);
            else
                CloudyRecConnector.Instance.HttpDelete(GetAppKey(), "fstore", id, null, callbackFunction);
        }
    }

    //Default callback functions for all the requests
    protected int callbackDelete(RestResponse response)
    {
        if ((int)response.StatusCode == 204)
            Debug.WriteLine("File deleted successfully");
        else
            Debug.WriteLine(response.Content);

        return 0;
    }

    protected int callbackUpload(RestResponse response)
    {
        if ((int)response.StatusCode == 201)
            Debug.WriteLine("File Uploaded/Updated successfully");
        else
            Debug.WriteLine(response.Content);

        return 0;
    }

    protected int callbackDownload(RestResponse response)
    {
        if ((int)response.StatusCode == 200)
        {
            Debug.WriteLine("File Downloaded successfully");
            Debug.WriteLine(response.Content);
        }
        else
            Debug.WriteLine(response.Content);

        return 0;
    }
}
