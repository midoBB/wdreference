using System;
using System.Windows;
using System.Net;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;

using Hashtable = System.Collections.Generic.Dictionary<object, object>;
using ArrayList = System.Collections.Generic.List<object>;

public enum HttpType
{
    GET,
    POST,
    PUT,
    DELETE,
}

public class CloudyRecConnector
{
    private string consumerKey = "oXveU63jaYhtkfCR41x8ww";
    private string consumerSecret = "hj1qHr8bDXrEDaAdcX5qgQ";
    private string version = "1";
    private string scheme = "http://";
    private string host = "api.cloudyrec.com";
    private int port = 80;
    private static CloudyRecConnector instance;

    private CloudyRecConnector() { }

    public static CloudyRecConnector Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new CloudyRecConnector();
            }
            return instance;
        }
    }

    //Call HTTP RestAPI services
    public void HttpGet(string appKey, string resourceKey, string id, Hashtable data = null, Func<RestResponse, int> callbackFunction = null)
    {
        HttpASyncCall(HttpType.GET, appKey, resourceKey, id, data, callbackFunction);
    }

    public void HttpPost(string appKey, string resourceKey, string id, Hashtable data = null, Func<RestResponse, int> callbackFunction = null)
    {
        HttpASyncCall(HttpType.POST, appKey, resourceKey, id, data, callbackFunction);
    }

    public void httpPostMultipart(string appKey, string resourceKey, string id, byte[] data, String fileName, String contentType, Func<RestResponse, int> callbackFunction = null) 
    {
        HttpASyncCallFileUpload(HttpType.POST, appKey, resourceKey, id, data, fileName, contentType);
	}

    public void HttpPut(string appKey, string resourceKey, string id, Hashtable data = null, Func<RestResponse, int> callbackFunction = null)
    {
        HttpASyncCall(HttpType.PUT, appKey, resourceKey, id, data, callbackFunction);
    }

    public void httpPutMultipart(string appKey, string resourceKey, string id, byte[] data, String fileName, String contentType, Func<RestResponse, int> callbackFunction = null)
    {
        HttpASyncCallFileUpload(HttpType.PUT, appKey, resourceKey, id, data, fileName, contentType);
	}

    public void HttpDelete(string appKey, string resourceKey, string id, Hashtable data = null, Func<RestResponse, int> callbackFunction = null)
    {
        HttpASyncCall(HttpType.DELETE, appKey, resourceKey, id, data, callbackFunction);
    }

    //Async Operations for HTTP request
    protected void HttpASyncCall(HttpType type, string appKey, string resourceKey, string id, Hashtable data = null, Func<RestResponse, int> callbackFunction = null)
    {
        var client = new RestClient();
        RestRequest request = null;

        string url = BuildURL(appKey, resourceKey, id, data);
	client.Authenticator = OAuth1Authenticator.ForProtectedResource(consumerKey, consumerSecret, "", "");	

        if (type == HttpType.GET)
        {
            //Need different URL everytime to avoid caching in windows phone 7
            DateTime date = DateTime.Now;
            if (data == null)
                url += "?rand=" + date.ToString();
            else
                url += "&rand=" + date.ToString();
        }

        //Start RestResponse from the server
        switch (type)
        {
            case HttpType.GET: request = new RestRequest(url, Method.GET); break;
            case HttpType.POST: request = new RestRequest(url, Method.POST); break;
            case HttpType.PUT: request = new RestRequest(url, Method.PUT); break;
            case HttpType.DELETE: request = new RestRequest(url, Method.DELETE); break;
        }

        client.ExecuteAsync(request, (response) =>
        {
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                if (callbackFunction != null)
                    callbackFunction(response);
                else
                    callbackDefault(response);
            }
            else if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (callbackFunction != null)
                    callbackFunction(response);
                else
                    callbackDefault(response);
            }
        });
    }

    protected void HttpASyncCallFileUpload(HttpType type, string appKey, string resourceKey, string id, byte[] data, String fileName, String contentType, Func<RestResponse, int> callbackFunction = null)
    {
        var client = new RestClient();
        RestRequest request = null;

        string url = BuildURL(appKey, resourceKey, id, null);
	client.Authenticator = OAuth1Authenticator.ForProtectedResource(consumerKey, consumerSecret, "", "");

        //Start RestResponse from the server
        switch (type)
        {
            case HttpType.POST: request = new RestRequest(url, Method.POST); break;
            case HttpType.PUT: request = new RestRequest(url, Method.PUT); break;
        }

        request.AddFile("file", data, fileName, contentType);
        client.ExecuteAsync(request, (response) =>
        {
            if (response.ResponseStatus == ResponseStatus.Error)
            {
                if (callbackFunction != null)
                    callbackFunction(response);
                else
                    callbackDefault(response);
            }
            else if (response.ResponseStatus == ResponseStatus.Completed)
            {
                if (callbackFunction != null)
                    callbackFunction(response);
                else
                    callbackDefault(response);
            }
        });
    }

    public string GetFileUrl(string appKey, string id)
    {
        string url = scheme + host + ":" + port.ToString() + "/" + version + "/" + appKey + "/fstore/" + id;
        return url;
    }

    private string BuildURL(string appKey, string resourceKey, string id, Hashtable data)
    {
        string url = scheme + host + ":" + port.ToString() + "/" + version + "/" + appKey + "/" + resourceKey + "/" + id;

        if (id == null || id == "")
            url = scheme + host + ":" + port.ToString() + "/" + version + "/" + appKey + "/" + resourceKey;

        if (data != null)
        {
            int i = 0;

            foreach (KeyValuePair<Object, Object> pair in data)
            {
                if (i == 0)
                    url += "?" + pair.Key + "=" + pair.Value;
                else
                    url += "&" + pair.Key + "=" + pair.Value;

                i++;
            }
        }

        //Save the string as URI
        url.Replace("\r\n", "\n");
        //Debug.WriteLine(url);

        url = System.Uri.EscapeUriString(url).Replace("#", "%23");
        Debug.WriteLine(url);

        return url;
    }

    //Default callback functions for all the requests
    protected int callbackDefault(RestResponse response)
    {
        Debug.WriteLine(response.Content);
        return 0;
    }

}
