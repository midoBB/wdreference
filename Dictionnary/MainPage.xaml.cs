﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.Windows.Media.Imaging;

namespace Dictionnary
{
    public partial class MainPage : PhoneApplicationPage
    {
        public MainPage()
        {
            Telerik.Windows.Controls.InteractionEffectManager.AllowedTypes.Add(typeof(TextBlock));
            InitializeComponent();
        }


        private void Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TextBlock block = sender as TextBlock;
            List<FlurryWP7SDK.Models.Parameter> Params = new List<FlurryWP7SDK.Models.Parameter> { new FlurryWP7SDK.Models.Parameter("Selected Dict", block.Name) };
            FlurryWP7SDK.Api.LogEvent("Dictionnary selected", Params);
            ExpanderView parent = block.Parent as ExpanderView;
            StackPanel o = parent.Expander as StackPanel;
            Image i = o.Children.First() as Image;
            NavigationService.Navigate(new Uri("/PivotPage1.xaml?dict=" + block.Name+"&display="+block.Text+"&icon="+(i.Source as  BitmapImage).UriSource, UriKind.Relative));
            
        }
        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            FlurryWP7SDK.Api.LogEvent("Other Apps Checked");
            NavigationService.Navigate(new Uri("/OtherApps.xaml", UriKind.Relative));
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {

            FlurryWP7SDK.Api.LogEvent("About checked");
            NavigationService.Navigate(new Uri("/About.xaml", UriKind.Relative));
        }
    }
}