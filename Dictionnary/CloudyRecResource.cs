using System;
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Hashtable = System.Collections.Generic.Dictionary<object, object>;
using ArrayList = System.Collections.Generic.List<object>;

public class CloudyRecResource
{
    private string appKey = "RB7NArWpHI";

    public string id { get; set; }
    public string rivaledgeId { get; set; }

    // Use this for initialization
    public CloudyRecResource(){}

    public string GetAppKey()
    {
        return appKey;
    }

    public string GetID()
    {
        return id;
    }

    //Overriden by sub-classes
    public virtual string GetResourceKey()
    {
        return null;
    }

    public virtual string GetRivalEdgeResourceKey()
    {
        return null;
    }

    protected void AuthenticateToCloud(string uname, string passwd, Func<RestResponse, int> callbackFunction = null)
    {
        Hashtable data = new Hashtable();
        data.Add("_uname", uname);
        data.Add("_passwd", passwd);

        CloudyRecConnector.Instance.HttpPost(appKey, GetResourceKey(), "auth", data, callbackFunction);
    }

    protected void InsertToCloud(Hashtable data, Func<RestResponse, int> callbackFunction = null)
    {
        CloudyRecConnector.Instance.HttpPost(appKey, GetResourceKey(), null, data, callbackFunction);
    }

    protected void UpdateToCloud(Hashtable data, Func<RestResponse, int> callbackFunction = null)
    {
        CloudyRecConnector.Instance.HttpPut(appKey, GetResourceKey(), id, data, callbackFunction);
    }

    protected void LoadFromCloud(string id, Func<RestResponse, int> callbackFunction = null)
    {
        //Record not found error if the id is not exised
        CloudyRecConnector.Instance.HttpGet(appKey, GetResourceKey(), id, null, callbackFunction);
    }

    protected void DeleteFromCloud(Func<RestResponse, int> callbackFunction = null)
    {
        CloudyRecConnector.Instance.HttpDelete(appKey, GetResourceKey(), id, null, callbackFunction);
    }

    protected void ListFromCloud(string query, string orderBy, bool ascending, int limit, int page, Func<RestResponse, int> callbackFunction)
    {
        if (limit < 0)
            limit = 10;
        if (page <= 0)
            page = 1;

        Hashtable data = new Hashtable();
        data.Add("limit", limit);
        data.Add("page", page);

        //Response data from the cloud
        if (query == null && orderBy == null)
        {
            CloudyRecConnector.Instance.HttpGet(appKey, GetResourceKey(), null, data, callbackFunction);
        }
        else
        {
            //Save the string as URI
            //string queryEscape = System.Uri.EscapeUriString(query);

            Debug.WriteLine("Query is " + query);

            data.Add("query", query);
            data.Add("ob", orderBy);

            if (ascending)
                data.Add("o", "1");
            else
                data.Add("o", "-1");

            CloudyRecConnector.Instance.HttpGet(appKey, GetResourceKey(), null, data, callbackFunction);
        }
    }

    protected void ListChildren(string query, string orderBy, bool ascending, string resourceKey, int limit = 10, int page = 0, Func<RestResponse, int> callbackFunction = null)
    {
        if (limit < 0)
            limit = 10;

        if (page <= 0)
            page = 1;

        Hashtable data = new Hashtable();
        data.Add("limit", limit);
        data.Add("page", page);

        //Response data from the cloud
        //Pass Child Resource Key
        if (query == null && orderBy == null)
        {
            CloudyRecConnector.Instance.HttpGet(appKey, resourceKey + "/cof", id, data, callbackFunction);
        }
        else
        {
            //Save the string as URI
            //string queryEscape = System.Uri.EscapeUriString(query);

            data.Add("query", query);
            data.Add("ob", orderBy);

            if (ascending)
                data.Add("o", "1");
            else
                data.Add("o", "-1");

            CloudyRecConnector.Instance.HttpGet(appKey, resourceKey + "/cof", id, data, callbackFunction);
        }
    }
}
