﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.IO.IsolatedStorage;
using System.IO;

namespace Dictionnary
{
    public class FileUtilities
    {
        public static void CreateDirectory(string directoryName)
        {
            try
            {
                IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
                if (!string.IsNullOrEmpty(directoryName) && !myIsolatedStorage.DirectoryExists(directoryName))
                {
                    myIsolatedStorage.CreateDirectory(directoryName);
                }
            }
            catch
            {
                // handle the exception
            }
        }
        public static string[] lsDirectory(string directory, string filetype)
        {
            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            return myIsolatedStorage.GetFileNames(directory+"\\*."+filetype);
        }
        public static IsolatedStorageFileStream getFiletoRead(string directory, string name) 
        {
            IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFileStream fileStream;
            if(myIsolatedStorage.FileExists(directory+"/"+name)){
            fileStream = myIsolatedStorage.OpenFile(directory+"/"+name, FileMode.Open);
            }
            else{
                StreamWriter writeFile = new StreamWriter(new IsolatedStorageFileStream(directory + "\\" + name, FileMode.CreateNew, myIsolatedStorage));
                writeFile.WriteLine();
                writeFile.Close();
                fileStream  = myIsolatedStorage.OpenFile(directory+"\\"+name, FileMode.Open, FileAccess.Read);
            }
            return fileStream;
        }
        public static StreamWriter getFiletoSave(string directory,string name,FileMode mode)
        {
            StreamWriter writeFile = new StreamWriter(System.IO.Stream.Null);
            try
            {
                IsolatedStorageFile myIsolatedStorage = IsolatedStorageFile.GetUserStoreForApplication();
                
                FileUtilities.CreateDirectory(directory);
                if (!myIsolatedStorage.FileExists(directory + "/" + name))
                    myIsolatedStorage.CreateFile(directory + "/" + name);
                writeFile = new StreamWriter(myIsolatedStorage.OpenFile(directory+"/"+name,mode));
                return writeFile;

            }
            catch 
            {
            }
            return writeFile;
        }
    }
}
