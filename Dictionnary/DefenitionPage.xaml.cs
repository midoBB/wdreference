﻿
using System.Xml.Linq;
using Hammock;
using Newtonsoft.Json;
using Microsoft.Phone.Controls;
using System.Windows;
using HtmlAgilityPack;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net;
using System;
using Microsoft.Phone.Shell;

namespace Dictionnary
{
    public partial class DefinitionPage : PhoneApplicationPage
    {
        HtmlNode Node;
        string dict = "";
        string req = "";
        string filename = "";
        string reponsecontent = "";
        String soundPath;
        Microsoft.Phone.Shell.ProgressIndicator indicator;
        // Constructor
        public DefinitionPage()
        {
            InitializeComponent();
            indicator = new Microsoft.Phone.Shell.ProgressIndicator();
            indicator.IsIndeterminate = true;
            indicator.IsVisible = true;
            indicator.Text = "Loading the definition";
            Microsoft.Phone.Shell.SystemTray.SetProgressIndicator(this, indicator);
            Loaded += new RoutedEventHandler(DefinitionPage_Loaded);
        }

        void DefinitionPage_Loaded(object sender, RoutedEventArgs e)
        {
            System.Threading.Timer timer = new System.Threading.Timer((object o) =>
            {
                Dispatcher.BeginInvoke(() => adControl.Visibility = System.Windows.Visibility.Visible);
            }, null, 500, System.UInt32.MaxValue);
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (NavigationContext.QueryString.TryGetValue("dict", out dict) && NavigationContext.QueryString.TryGetValue("req", out req))
            {
                RestClient client = new RestClient();
                RestRequest request = new RestRequest()
                {
                    Path = "http://api.wordreference.com/df1f6/" + dict + "/" + req
                };
                //RestRequest soundrequest = new RestRequest()
                //{
                //    Path = "http://apifree.forvo.com/action/word-pronunciations/format/json/word/" + req + "/key/44fd2391495ca8ea6ba0fb6c55f47025/"
                //};
                client.BeginRequest(request, new RestCallback(TraductionCompleted));
                WebClient cl = new WebClient();
                cl.DownloadStringCompleted += new DownloadStringCompletedEventHandler(cl_DownloadStringCompleted);
                cl.DownloadStringAsync(new Uri("http://apifree.forvo.com/action/word-pronunciations/format/json/word/" + req + "/key/44fd2391495ca8ea6ba0fb6c55f47025/"));
                //client.BeginRequest(soundrequest, new RestCallback(PronounceCompleted));
            }
            else if (NavigationContext.QueryString.TryGetValue("dict", out dict) && NavigationContext.QueryString.TryGetValue("savedfilename",out filename))
            {
                ApplicationBar.IsVisible = false;
                var settings = IsolatedStorageSettings.ApplicationSettings;
                string reading = "";
                settings.TryGetValue((dict + ":" + filename),out reading);
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                WebClient cl = new WebClient();
                cl.DownloadStringCompleted += new DownloadStringCompletedEventHandler(cl_DownloadStringCompleted);
                cl.DownloadStringAsync(new Uri("http://apifree.forvo.com/action/word-pronunciations/format/json/word/" + req + "/key/44fd2391495ca8ea6ba0fb6c55f47025/"));
                NavigationService.RemoveBackEntry();
                if (reading != null)
                {
                    string s = reading.Replace("<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\">", "<meta name=\"viewport\" content=\"width=300,user-scalable=no\" /> \n <meta http-equiv=\"Content-type\" content=\"text/html;charset=iso-8859-1\">");
                    doc.LoadHtml(s);
                    Node = doc.DocumentNode;
                    HandleNode(Node);
                }
            }
        }
        
        void cl_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            try
            {
                XDocument doc = JsonConvert.DeserializeXNode(e.Result, "doc");
                soundPath = doc.Root.Element("items").
                    Element("pathmp3").Value;
                (ApplicationBar.Buttons[0] as ApplicationBarIconButton).IsEnabled = true;
            }
            catch
            {
                ApplicationBarIconButton pronounceButton = ApplicationBar.Buttons[0] as ApplicationBarIconButton;
                pronounceButton.IsEnabled = false;
            }
        }
        private void TraductionCompleted(RestRequest request, RestResponse response, object userstate)
        {
            //     string responsewithoutnumbers = response.Content.Replace("0", "content").Replace("1", "content").Replace("2", "content").Replace("3", "content").Replace("4", "content").Replace("5", "content").Replace("6", "content").Replace("7", "content").Replace("8", "content").Replace("9", "content").Replace("contentcontent", "content");
            //    XDocument doc = JsonConvert.DeserializeXNode(responsewithoutnumbers,"doc");
            reponsecontent = response.Content;
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            string s = response.Content.Replace("<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\">", "<meta name=\"viewport\" content=\"width=300,user-scalable=no\" /> \n <meta http-equiv=\"Content-type\" content=\"text/html;charset=iso-8859-1\">");
            doc.LoadHtml(s);
            Node = doc.DocumentNode;
            HandleNode(Node);
        }
        private void HandleNode(HtmlNode node)
        {
            if (node != null)
            {

                if (node.Name == "div")
                {
                    if (node.InnerHtml.Contains("WordReference.com Dictionaries") && node.XPath == "/html[1]/body[1]/div[1]/div[1]")
                    {
                        node.RemoveAll();
                    }
                    if (node.InnerHtml.Contains("form") && node.XPath == "/html[1]/body[1]/div[1]/div[2]")
                        node.RemoveAll();

                }
                if (node.Name == "table")
                {
                    try
                    {
                        foreach (var atr in node.Attributes)
                        {
                            if (atr.Name == "class" && atr.Value == "WRreporterror") { node.RemoveAll(); }
                        }
                    }
                    catch { }
                }
                if (node.Name == "a") { node.RemoveAll(); }
                if (node.ParentNode != null && Node.LastChild == node)
                    Dispatcher.BeginInvoke(() =>
                    {
                        indicator.IsVisible = false;
                        browser.NavigateToString(Node.InnerHtml);
                        browser.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
                    });
                foreach (HtmlNode childNode in node.ChildNodes)
                {
                    HandleNode(childNode);
                }

            }

        }

        private void ApplicationBarMenuItem_Click(object sender, System.EventArgs e)
        {
            var settings = IsolatedStorageSettings.ApplicationSettings;
            Coding4Fun.Phone.Controls.ToastPrompt toast;
            if (!settings.Contains(dict + ":" + req))
            {
                settings.Add(dict + ":" + req, reponsecontent);
                toast = new Coding4Fun.Phone.Controls.ToastPrompt() { Title = "Saved Succesfully", Message = "The current document is saved", TextOrientation = System.Windows.Controls.Orientation.Vertical };
            }
            else { toast = new Coding4Fun.Phone.Controls.ToastPrompt() { Title = "Alredy Saved", Message = "The current document is saved", TextOrientation = System.Windows.Controls.Orientation.Vertical }; }
            toast.Show();
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            sound.Source = new Uri(soundPath);
            sound.Play();
        }
    }
}
