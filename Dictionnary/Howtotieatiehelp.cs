using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Hashtable = System.Collections.Generic.Dictionary<object, object>;
using ArrayList = System.Collections.Generic.List<object>;

public class Howtotieatiehelp : CloudyRecResource
{
    private string howtotieatiehelpResKey = "ZBqNtrDwNn";
	public string help { get; set;}


    //Gamer Resource Key
    public override string GetResourceKey() { return howtotieatiehelpResKey; }

    public Howtotieatiehelp(){}

    public Howtotieatiehelp(string help) {
		this.help = help;
    }



    public void Load(string id, Func<RestResponse, int> callbackFunction = null)
    {
            if (callbackFunction == null)
                LoadFromCloud(id, CallbackLoad);
            else
                LoadFromCloud(id, callbackFunction);
    }

    public void Save(Func<RestResponse, int> callbackFunction = null)
    {
        Hashtable data = new Hashtable();
	data.Add("help", this.help);

        if (this.id == null)
        {
                if (callbackFunction != null)
                    InsertToCloud(data, callbackFunction);
                else
                    InsertToCloud(data, CallbackSave);
        }
        else
        {
                if (callbackFunction != null)
                    UpdateToCloud(data, callbackFunction);
                else
                    UpdateToCloud(data, CallbackSave);
        }        
    }

    public void Delete(Func<RestResponse, int> callbackFunction = null)
    {
        if (callbackFunction != null)
            DeleteFromCloud(callbackFunction);
        else
            DeleteFromCloud(CallbackDelete);
    }

    //Call back from this class
    public void HowtotieatiehelpList(string query, string orderBy, bool ascending, int limit = 10, int page = 1, Func<RestResponse, int> callbackFunction = null)
    {
        //Start calling the list from cloud in another thread
        if(callbackFunction != null)
            ListFromCloud(query, orderBy, ascending, limit, page, callbackFunction);
        else
		ListFromCloud(query, orderBy, ascending, limit, page, CallbackHowtotieatiehelpList);
    }

    public void SetData(JObject data)
    {
            this.id = (string)data["id"];
		this.help = (String)data["help"];
    }

    private int CallbackHowtotieatiehelpList(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        //Debug.WriteLine(response.Content);
	ArrayList objList = new ArrayList();
        JArray objArray = JArray.Parse(response.Content);

        foreach (JObject data in objArray)
        {
            Howtotieatiehelp obj = new Howtotieatiehelp();
	    obj.SetData(data);
            objList.Add(obj);
        }
        return 0;
    }
	


    //UserAccount Load Callback
    private int CallbackLoad(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        Debug.WriteLine(response.Content);
        JObject data = JObject.Parse(response.Content);
        SetData(data);
        return 0;
    }

    //UserAccount Save Callback
    private int CallbackSave(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        Debug.WriteLine(response.Content);
	id = response.Content;
        return 0;
    }

    //UserAccount Delete Callback
    private int CallbackDelete(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        Debug.WriteLine(response.Content);
	
	id = null;
        return 0;
    }
}
