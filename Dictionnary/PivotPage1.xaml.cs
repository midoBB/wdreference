﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO.IsolatedStorage;
using System.IO;
using Microsoft.Phone.Shell;

namespace Dictionnary
{
    public partial class PivotPage1 : PhoneApplicationPage
    {
        public string dict = "";
        public string iconpath = "";
        public PivotPage1()
        {
            Telerik.Windows.Controls.InteractionEffectManager.AllowedTypes.Add(typeof(TextBlock));
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            string display;
            if (NavigationContext.QueryString.TryGetValue("display", out display))
            { Pivot1.Title = display; }
            NavigationContext.QueryString.TryGetValue("dict", out dict);
            NavigationContext.QueryString.TryGetValue("icon", out iconpath);
            FileUtilities.CreateDirectory(dict);
            var settings = IsolatedStorageSettings.ApplicationSettings;
            System.Collections.IEnumerator enumerator = settings.Keys.GetEnumerator();
            System.Collections.ObjectModel.ObservableCollection<string> savedCollection = new System.Collections.ObjectModel.ObservableCollection<string>();
            while (enumerator.MoveNext()) if ((enumerator.Current as string).StartsWith(dict) && (enumerator.Current as string).Split(':').Length >= 1) savedCollection.Add((enumerator.Current as string).Split(':')[1]);
            listbox.ItemsSource = savedCollection;
            using (StreamReader reader = new StreamReader(FileUtilities.getFiletoRead(dict, "recent.txt")))
            {    //Visualize the text data in a TextBlock text
                string reading = reader.ReadToEnd();
                try
                {
                    recent1.Text = reading.Substring(reading.LastIndexOf('\n')).Trim();
                    reading = reading.Remove(reading.LastIndexOf("\n"));
                    recent1.Text = reading.Substring(reading.LastIndexOf('\n')).Trim();
                    reading = reading.Remove(reading.LastIndexOf("\n"));
                    recent2.Text = reading.Substring(reading.LastIndexOf('\n')).Trim();
                    reading = reading.Remove(reading.LastIndexOf("\n"));
                    recent3.Text = reading.Substring(reading.LastIndexOf('\n')).Trim();
                    reading = reading.Remove(reading.LastIndexOf("\n"));
                    recent4.Text = reading.Substring(reading.LastIndexOf('\n')).Trim();
                    reading = reading.Remove(reading.LastIndexOf("\n"));
                    recent5.Text = reading.Substring(reading.LastIndexOf('\n')).Trim();
                    reading = reading.Remove(reading.LastIndexOf("\n"));
                }
                catch { }
                reader.Close();
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            using (StreamWriter writer = FileUtilities.getFiletoSave(dict, "recent.txt", FileMode.Append))
            {
                if (searchBox.Text != recent1.Text && searchBox.Text != recent2.Text && searchBox.Text != recent3.Text && searchBox.Text != recent4.Text && searchBox.Text != recent5.Text)
                {
                    writer.WriteLine(searchBox.Text);
                    writer.Close();
                }
            }
            NavigationService.Navigate(new Uri("/DefenitionPage.xaml?dict=" + dict + "&req=" + searchBox.Text, UriKind.Relative));
        }

        private void searchBox_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            expander.IsExpanded = false;
        }

        private void recent_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            TextBlock block = sender as TextBlock;
            List<FlurryWP7SDK.Models.Parameter> Params = new List<FlurryWP7SDK.Models.Parameter> { new FlurryWP7SDK.Models.Parameter("Search terms", block.Text) };
            FlurryWP7SDK.Api.LogEvent("Search started", Params);
            NavigationService.Navigate(new Uri("/DefenitionPage.xaml?dict=" + dict + "&req=" + block.Text, UriKind.Relative));
        }

        private void listbox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox box = sender as ListBox;
            FlurryWP7SDK.Api.LogEvent("Saved File selected");
            NavigationService.Navigate(new Uri("/DefenitionPage.xaml?dict=" + dict + "&savedfilename=" + box.SelectedItem, UriKind.Relative));
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            var foundTile = ShellTile.ActiveTiles.FirstOrDefault(x => x.NavigationUri.ToString().Contains("dict=" + dict));

            if (foundTile == null)
            {
                var secondaryTile = new StandardTileData
                                        {
                                            BackgroundImage = new Uri(iconpath, UriKind.Relative),
                                            Title = Pivot1.Title as String,
                                            Count = null
                                        };

                ShellTile.Create(new Uri("/PivotPage1.xaml?dict=" + dict + "&display=" + Pivot1.Title+"&icon="+iconpath, UriKind.Relative), secondaryTile);
            }
        }
    }
}
