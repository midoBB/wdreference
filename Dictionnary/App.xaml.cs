﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

using Newtonsoft.Json.Linq;
using RestSharp;
using ArrayList = System.Collections.Generic.List<object>;
namespace Dictionnary
{
    public partial class App : Application
    {
        public Setting<bool> showAnnouncments = new Setting<bool>("showAnnouncments", true);
        public Setting<string> lastSHownAnnouncment = new Setting<string>("lastshownAnnouncment", "");
        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public Telerik.Windows.Controls.RadPhoneApplicationFrame RootFrame { get; private set; }
        public const string ApiKeyValue = "DQWA3C671EI869HESG3J";
        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
             //   Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode, 
                // which shows areas of a page that are being GPU accelerated with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;
            }

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();
            ThemeManager.ToLightTheme();
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            FlurryWP7SDK.Api.StartSession(ApiKeyValue);
            FlurryWP7SDK.Api.LogPageView();
            if (showAnnouncments.Value == true && ReviewBugger.numOfRuns > 1)
            {
                Announcment anouncment = new Announcment();
                anouncment.AnnouncmentList(null, null, true, 10, 1, callbackFunction);
            }
        }
        private int callbackFunction(RestResponse response)
        {
            try
            {
                ArrayList objList = new ArrayList();
                JArray objArray = JArray.Parse(response.Content);
                foreach (JObject data in objArray)
                {
                    Announcment obj = new Announcment();
                    obj.SetData(data);
                    objList.Add(obj);
                }
                Announcment anouncment = (Announcment)objList.Last();
                if (lastSHownAnnouncment.Value != anouncment.id)
                {
                    Telerik.Windows.Controls.RadMessageBox.Show(anouncment.title, Telerik.Windows.Controls.MessageBoxButtons.YesNo, anouncment.text, "don't show me your new apps anymore", false, false, HorizontalAlignment.Stretch, VerticalAlignment.Top,
                        (args) =>
                        {
                            if (args.IsCheckBoxChecked)
                            {
                                showAnnouncments.Value = false;
                            }
                            if (args.Result == Telerik.Windows.Controls.DialogResult.OK)
                            {
                                Microsoft.Phone.Tasks.MarketplaceDetailTask task = new Microsoft.Phone.Tasks.MarketplaceDetailTask();
                                task.ContentIdentifier = anouncment.apuri;
                                task.Show();
                            }
                        });
                    lastSHownAnnouncment.Value = anouncment.id;
                }
            }
            catch { }
            return 0;
        }
        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            FlurryWP7SDK.Api.StartSession(ApiKeyValue);
            FlurryWP7SDK.Api.LogPageView();
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
			// Ensure that required application state is persisted here.
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            List<FlurryWP7SDK.Models.Parameter> Params = new List<FlurryWP7SDK.Models.Parameter> { new FlurryWP7SDK.Models.Parameter("Message", e.Exception.Message), new FlurryWP7SDK.Models.Parameter("Stack", e.Exception.StackTrace) };
            FlurryWP7SDK.Api.LogEvent("Error occured", Params);
            FlurryWP7SDK.Api.LogError("navigation error", e.Exception);
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            List<FlurryWP7SDK.Models.Parameter> Params = new List<FlurryWP7SDK.Models.Parameter> { new FlurryWP7SDK.Models.Parameter("Message", e.ExceptionObject.Message), new FlurryWP7SDK.Models.Parameter("Stack", e.ExceptionObject.StackTrace) };
            FlurryWP7SDK.Api.LogEvent("Error occured", Params);
            FlurryWP7SDK.Api.LogError(e.ExceptionObject.Message, e.ExceptionObject);
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            Telerik.Windows.Controls.RadPhoneApplicationFrame root = new Telerik.Windows.Controls.RadPhoneApplicationFrame();
            Telerik.Windows.Controls.RadTurnstileTransition anim = new Telerik.Windows.Controls.RadTurnstileTransition();
            root.OrientationChangeAnimation = new Telerik.Windows.Controls.RadTileAnimation();
            root.Transition = anim;
            RootFrame = root;
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        #endregion
    }
}
