using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Security.Cryptography.X509Certificates;

public class CloudyRecException : Exception
{
    public CloudyRecException(string message)
        : base(message)
    {

    }
}
