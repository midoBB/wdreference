﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Hashtable = System.Collections.Generic.Dictionary<object, object>;
using ArrayList = System.Collections.Generic.List<Myapps>;
using System.Collections.ObjectModel;


namespace _5Min
{
    public partial class OtherApps : PhoneApplicationPage
    {
        ObservableCollection<Myapps> Items = new ObservableCollection<Myapps>();
        public OtherApps()
        {
            InitializeComponent();
            Myapps apps = new Myapps();
            apps.MyappsList(null, null, true, 10, 1, CallbackUserAccountList);
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            flag = false;
        }
        private int CallbackUserAccountList(RestResponse response)
        {
            try
            {
                JArray objArray = JArray.Parse(response.Content);
                foreach (JObject data in objArray)
                {
                    Myapps obj = new Myapps();
                    obj.SetData(data);
                    Items.Add(obj);
                }
                ListB.ItemsSource = this.Items;
            }
            catch { }
            return 0;
        }
        bool flag = false;
        private void ListB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!flag)
            {
                Microsoft.Phone.Tasks.MarketplaceDetailTask task = new Microsoft.Phone.Tasks.MarketplaceDetailTask();
                task.ContentIdentifier = Items.ElementAt(ListB.SelectedIndex).url;
                task.Show();
            }
            flag = true;
        }
    }
}
