using System;
using System.Windows;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Hashtable = System.Collections.Generic.Dictionary<object, object>;
using ArrayList = System.Collections.Generic.List<object>;

public class Announcment : CloudyRecResource
{
    private string announcmentResKey = "tBmNPrRwQ4";
	public string text { get; set;}
	public string title { get; set;}
	public string apuri { get; set;}
	public DateTime expierydate { get; set;}


    //Gamer Resource Key
    public override string GetResourceKey() { return announcmentResKey; }

    public Announcment(){}

    public Announcment(string text, string title, string apuri, DateTime expierydate) {
		this.text = text;
		this.title = title;
		this.apuri = apuri;
		this.expierydate = expierydate;
    }



    public void Load(string id, Func<RestResponse, int> callbackFunction = null)
    {
            if (callbackFunction == null)
                LoadFromCloud(id, CallbackLoad);
            else
                LoadFromCloud(id, callbackFunction);
    }

    public void Save(Func<RestResponse, int> callbackFunction = null)
    {
        Hashtable data = new Hashtable();
	data.Add("text", this.text);
	data.Add("title", this.title);
	data.Add("apuri", this.apuri);
	data.Add("expierydate",this.expierydate);

        if (this.id == null)
        {
                if (callbackFunction != null)
                    InsertToCloud(data, callbackFunction);
                else
                    InsertToCloud(data, CallbackSave);
        }
        else
        {
                if (callbackFunction != null)
                    UpdateToCloud(data, callbackFunction);
                else
                    UpdateToCloud(data, CallbackSave);
        }        
    }

    public void Delete(Func<RestResponse, int> callbackFunction = null)
    {
        if (callbackFunction != null)
            DeleteFromCloud(callbackFunction);
        else
            DeleteFromCloud(CallbackDelete);
    }

    //Call back from this class
    public void AnnouncmentList(string query, string orderBy, bool ascending, int limit = 10, int page = 1, Func<RestResponse, int> callbackFunction = null)
    {
        //Start calling the list from cloud in another thread
        if(callbackFunction != null)
            ListFromCloud(query, orderBy, ascending, limit, page, callbackFunction);
        else
		ListFromCloud(query, orderBy, ascending, limit, page, CallbackAnnouncmentList);
    }

    public void SetData(JObject data)
    {
            this.id = (string)data["id"];
		this.text = (String)data["text"];
		this.title = (String)data["title"];
		this.apuri = (String)data["apuri"];
		this.expierydate = DateTime.Parse((String) data["expierydate"]);
    }

    private int CallbackAnnouncmentList(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        //Debug.WriteLine(response.Content);
	ArrayList objList = new ArrayList();
        JArray objArray = JArray.Parse(response.Content);

        foreach (JObject data in objArray)
        {
            Announcment obj = new Announcment();
	    obj.SetData(data);
            objList.Add(obj);
        }
        return 0;
    }
	


    //UserAccount Load Callback
    private int CallbackLoad(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        Debug.WriteLine(response.Content);
        JObject data = JObject.Parse(response.Content);
        SetData(data);
        return 0;
    }

    //UserAccount Save Callback
    private int CallbackSave(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        Debug.WriteLine(response.Content);
	id = response.Content;
        return 0;
    }

    //UserAccount Delete Callback
    private int CallbackDelete(RestResponse response)
    {
	Debug.WriteLine(response.StatusCode);
        Debug.WriteLine(response.Content);
	
	id = null;
        return 0;
    }
}
